<?php
/******************************************************
Titre  : Apprendre à manipuler les médias dans une BDD
Auteur : NGN TKD
Date   : 27 Janvier 2020 - Version 1.0
Desc.  : Page pour poster
*******************************************************/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Centre de Formation Professionnelle et Technique d'Informatique</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/facebook.css" rel="stylesheet">
</head>
<body>
    <h1>Poster</h1>
    
    <!--post modal-->
	<div class="modal-header">
		Update Status
    </div>
    <form class="form center-block" action="script/upload.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="form-group">
                <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?" name="comment"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <div>
            <ul class="pull-left list-inline"><input type="file" name="fileToUpload[]" accept="image/gif, image/jpeg, image/png, image/jpg, video/*, audio/*" multiple></li></ul>
                <input type="submit" name="submit" value="Post" class="btn btn-primary btn-sm">
            </div>	
        </div>
    </form>
    <a href="index.php">Accueil</a>
</body>
</html>