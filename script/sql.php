<?php
/******************************************************
Titre  : Apprendre à manipuler les médias dans une BDD
Auteur : NGN TKD
Date   : 27 Janvier 2020 - Version 1.0
Desc.  : Page de sql
Réf.   : https://www.php.net/manual/en/pdo.begintransaction.php
*******************************************************/
require_once("./constantes.php");

// Connection to the database
function connect(){
    static $db = null;

    if ($db == null) {
        try {
            $db = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME, DBUSER, DBPWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_PERSISTENT => true));
        } catch (Exception $e) {
            echo 'Error : ' . $e->getMessage() . '<br />';
            echo 'N° : ' . $e->getCode();
            die('Could not connect to MySQL');
        }
    }
    return $db;
 }

/* ---------------- C R U D S ---------------- */
    
// PDO::beginTransaction();

// Add a post
function AddPost($comment, $creationPost, $modificationPost){
    $sql = "INSERT INTO `Tbl_Post`(`Txt_Commentaire`, `Ts_CreationDate`, `Ts_ModificationDate`)
             VALUES(:comment, :creationPost, :modificationPost)";
   
    $request = connect()->prepare($sql);
    $request->bindParam(":comment", $comment, PDO::PARAM_STR);
    $request->bindParam(":creationPost", $creationPost, PDO::PARAM_STR);
    $request->bindParam(":modificationPost", $modificationPost, PDO::PARAM_STR);
    $request->execute();

    $result = $request->fetchAll(PDO::FETCH_ASSOC);

    return $result;
}
    
//PDO::commit();

// Add a media
function AddMedia($type, $mediaName, $creationMedia, $modificationMedia){
    $sql = "INSERT INTO `Tbl_Media`(`Txt_TypeMedia`, `Txt_NomMedia`, `Ts_CreationDate`, `Ts_ModificationDate`)
            VALUES(:type, :mediaName, :creationMedia, :modificationMedia)";
    
    $request = connect()->prepare($sql);
    $request->bindParam(":type", $type, PDO::PARAM_STR);
    $request->bindParam(":mediaName", $mediaName, PDO::PARAM_STR);
    $request->bindParam(":creationMedia", $creationMedia, PDO::PARAM_STR);
    $request->bindParam(":modificationMedia", $modificationMedia, PDO::PARAM_STR);
    $request->execute();

    $result = $request->fetchAll(PDO::FETCH_ASSOC);

    return $result;
}
    

?>