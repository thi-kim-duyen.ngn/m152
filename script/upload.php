<?php
/******************************************************
Titre  : Apprendre à manipuler les médias dans une BDD
Auteur : NGN TKD
Date   : 27 Janvier 2020 - Version 1.0
Desc.  : Page d'upload
*******************************************************/
require_once('./sql.php'); 

$comment = filter_input(INPUT_POST, "comment", FILTER_SANITIZE_STRING);

$creationDate = date('Y-m-d H:i:s');
$modificationDate = date('Y-m-d H:i:s');

$target_dir = "/var/www/html/m152/temp/";
$countfiles = count(array_filter($_FILES['fileToUpload']['name']));

$type = mime_content_type($_FILES['fileToUpload']['name']);

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {

    // Only upload if there's at least 1 file selected
    if ($countfiles != 0) {
        for($i = 0; $i < $countfiles; $i++){

            // Put time() so each imgs are unique
            $target_file = $target_dir . time() . '_' . basename($_FILES["fileToUpload"]["name"][$i]);
            
            // Display a msg if it's a success or a fail
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file)) {
                echo "The file ". basename( $_FILES["fileToUpload"]["name"][$i]) . " has been uploaded." . '<br>';
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    } else {
        header('Location: ../post.php');
        exit;
    }
    
    // Add a post
    // AddPost($comment, $creationDate, $modificationDate);

    // Add a media
    // AddMedia($type, $mediaName, $creationMedia, $modificationMedia);
}

    

?>